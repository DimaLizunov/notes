from django.contrib import admin
from notes_server.models import CategoryNote, LabelNote, Notes, Color
from django_mptt_admin.admin import DjangoMpttAdmin


class CategoryAdmin(DjangoMpttAdmin):
	list_display = ('category_name','parent')

admin.site.register(CategoryNote, CategoryAdmin)
admin.site.register(LabelNote)
admin.site.register(Color)

admin.site.register(Notes)

# Register your models here.
