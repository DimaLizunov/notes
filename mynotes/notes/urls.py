"""mynotes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from notes import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^note/$', views.MyNote.as_view()),
    url(r'^regist/$', views.Regist.as_view(), name = 'regist'),
    url(r'^login/$',  views.Login.as_view(), name = 'login'),
    url(r'^logout/$',  views.LogOut.as_view(), name = 'logout'),
    url(r'^$',  views.HomeView.as_view(), name = 'home'),
    url(r'^new_note/$', views.NewNote.as_view(), name = 'new_note'),
    url(r'^notes_list/(?P<pk>\d+)/del_note/$', views.NoteDelete.as_view(), name = 'del_note'),
    url(r'^new_category/$', views.NewCategory.as_view(), name = 'new_category'),
    url(r'^new_label/$', views.NewLabel.as_view(), name = 'new_label'),
    url(r'^new_color/$', views.NewColor.as_view(), name = 'new_color'),
    url(r'^notes_list/(?P<pk>\d+)/$',  views.GetNoteIdView.as_view(), name='notes_id_list'),
]
