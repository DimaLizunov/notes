from django.shortcuts import render
from notes_server.models import CategoryNote, LabelNote, Notes
from actions import my_note
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.views.generic import TemplateView, DetailView, ListView, FormView, View, DeleteView
from django.http import HttpResponse, HttpResponseRedirect
from notes_server.form import NoteForm, CategoryForm, LabelForm, ColorForm
from django.views.generic.edit import UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


class HomeView(TemplateView):
    template_name = 'notes/home.html'


class Regist(FormView):
    form_class = UserCreationForm
    success_url = '../login/'
    template_name = 'notes/regist.html'

    def form_valid(self, form):
        self.object = form.save()
        return super(Regist, self).form_valid(form)


class Login(FormView):
    form_class = AuthenticationForm
    success_url = '../note/'
    template_name = 'notes/login.html'

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(Login, self).form_valid(form)


class LogOut(View):
    success_url = '/regist/'
    template_name = 'notes/base.html'

    def get(self,request):
        logout(request)
        return HttpResponseRedirect('/login/')


class MyNote(ListView):
    template_name = 'notes/notes.html'
    model = Notes

    def get(self, request,*args,**kwargs):
        user = request.user
        note = user.notes_set.all()
        return render(request, self.template_name, {'note_list': note})


class NewCategory(FormView):
    form_class = CategoryForm
    success_url = '../note/'
    template_name = 'notes/new_category.html'

    def form_valid(self, form):
        self.object = form.save()
        return super(NewCategory, self).form_valid(form)


class NewLabel(FormView):
    form_class = LabelForm
    success_url = '../note/'
    template_name = 'notes/new_label.html'

    def form_valid(self, form):
        self.object = form.save()
        return super(NewLabel, self).form_valid(form)


class NewColor(FormView):
    form_class = ColorForm
    success_url = '../note/'
    template_name = 'notes/new_color.html'

    def form_valid(self, form):
        self.object = form.save()
        return super(NewColor, self).form_valid(form)


class NewNote(FormView):
    form_class = NoteForm
    success_url = '../note/'
    template_name = 'notes/new_note.html'

    def form_valid(self, form):
        form.instance.user = self.request.user
        self.object = form.save()
        return super(NewNote, self).form_valid(form)


class GetNoteIdView(UpdateView):
    model = Notes
    template_name = 'notes/notes_id_list.html'
    # template_name_suffix = '_update_form'
    success_url = '../../note/'
    fields = ['note_title', 'note_content']


class NoteDelete(DeleteView):
    model = Notes
    template_name = 'notes/del_note.html'
    success_url = '../../../note/'
