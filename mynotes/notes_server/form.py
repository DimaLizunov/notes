from django import forms
from models import Notes, CategoryNote, LabelNote, Color


class NoteForm(forms.ModelForm):
    class Meta:
        model = Notes
        fields = '__all__'


class CategoryForm(forms.ModelForm):
	class Meta:
		model = CategoryNote
		fields = '__all__'


class LabelForm(forms.ModelForm):
	class Meta:
		model = LabelNote
		fields = ('label_name',)
		

class ColorForm(forms.ModelForm):
	class Meta:
		model = Color
		fields = '__all__'
