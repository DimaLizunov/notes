from django.db import models
import mptt
from mptt.models import MPTTModel, TreeForeignKey
from django.contrib.auth.models import User
from colorful.fields import RGBColorField


class CategoryNote(models.Model):
	category_name = models.CharField(max_length=20, unique=True)
	parent = TreeForeignKey('self', null=True, blank=True, verbose_name="Parent",
		                     related_name='children', db_index=True)
	class MPTTMeta:
		order_insertion_by=['category_name']

	def __unicode__(self):
		return '{0}'.format(self.category_name)


mptt.register(CategoryNote, )


class LabelNote(models.Model):
	label_name = models.CharField(max_length=20)

	def __unicode__(self):
		return '{0}'.format(self.label_name)


class Color(models.Model):
    code = RGBColorField()
    
    def __unicode__(self):
        return '{0}'.format(self.code)
		

class Notes(models.Model):
	note_title = models.CharField(max_length=30)
	note_content = models.CharField(max_length=200)
	note_category = models.ManyToManyField(CategoryNote)
	note_label = models.ManyToManyField(LabelNote)
	note_pub_date = models.DateField(auto_now=True)
	user = models.ForeignKey(User)
	color = models.ForeignKey(Color, blank=True , null=True, related_name='color')
	

	def __unicode__(self):
		return '{0} {1}'.format(self.note_title, self.note_pub_date)